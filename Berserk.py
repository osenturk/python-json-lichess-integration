
import berserk
from datetime import datetime
import json
import time
import sys
import pandas as pd


input_file=sys.argv[1]

df=pd.read_csv(input_file,delimiter='=',comment='#',header=-1)

#print(df.head())

usernames=df.iloc[0,1].split(',')
file_path=df.iloc[1,1]
sleep_time=int(df.iloc[2,1])
how_many_games=int(int(df.iloc[3,1]))

#f = open("/home/prod/workspace/lichess/dict.json", "w")
f = open(file_path, "w")

ACTIVITY_FOLLOW='follows'
ACTIVITY_GAME='games'

def convert_to_date(int_date):
    readable = datetime.fromtimestamp(int_date/1000).isoformat(' ')
    readable = readable[:19]
    return readable

def convert_to_date_no_division_1000(int_date):
        readable = datetime.fromtimestamp(int_date).isoformat(' ')
        readable=readable[:10]
        return readable
    # 2012-08-29 11:38:22
    #return datetime.datetime.fromtimestamp(int(int_date)).strftime('%Y-%m-%d %H:%M:%S')

def format_activity(activity):
    new_activity={}

    #first layer for activities, either follows or games

    #when a game is saved, do not iterate the rest of the dictionary
    is_a_game_saved=False

    for interval in activity:

        for key_inner, value_inner in interval.items():

            if key_inner!=ACTIVITY_GAME:
                continue
            else:
                new_activity["online"] = convert_to_date_no_division_1000(interval["interval"]["start"])
                new_activity["offline"] = convert_to_date_no_division_1000(interval["interval"]["end"])
                key_for_games=next(iter(value_inner.keys()))
                new_activity["type"] = key_for_games
                new_activity["win"] = value_inner[key_for_games]["win"]
                new_activity["loss"] = value_inner[key_for_games]["loss"]
                new_activity["draw"] = value_inner[key_for_games]["draw"]
                new_activity["before"] = value_inner[key_for_games]["rp"]["before"]
                new_activity["after"] = value_inner[key_for_games]["rp"]["after"]
                is_a_game_saved=True

                # only get the first element of the activity
                break

        if(is_a_game_saved==True):
            break


    return  new_activity


def format_game(game):
    new_dict={}

    new_dict['time']=convert_to_date(game['createdAt'])
    new_dict['type'] = game['perf']
    new_dict['game_id']=game['id']
    new_dict['white_id'] = game['players']['white']['user']['id'][:9]
    new_dict['white_rating'] = game['players']['white']['rating']
    new_dict['black_id'] = game['players']['black']['user']['id'][:9]
    new_dict['black_rating'] = game['players']['black']['rating']

    if 'winner' in game:
        if game['winner'] == 'white':
            new_dict['status'] = 'white won'
        else:
            new_dict['status'] = 'black won'
    else:

        if game['status'] == 'outoftime':
            new_dict['status'] = 'draw by time'
        else:
            new_dict['status'] = game['status']



    new_dict['time_initial'] = int(game['clock']['initial']/60)
    new_dict['time_increment'] = game['clock']['increment']


    return new_dict

session = berserk.TokenSession('B2tDzx6LkIWTOm30')
lichess = berserk.Client(session)

all_games_for_users=[]


for username in usernames:

    print(username)
    games=lichess.games.export_by_player(username=username,as_pgn=False,max=how_many_games,rated='true',moves='false')
    activities=lichess.users.get_activity_feed(username=username)

    #each username is a dictionary and has a list of games and activity
    username_dict = {}
    format_activity_ = format_activity(activities)
    username_dict['username'] = username
    username_dict["activity"]=format_activity_
    username_dict["updated"]=datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    #each game and activity are tailored based on the requirements
    games_list = []



    for game in games:

        format_game_=format_game(game)
        games_list.append(format_game_)


    #cover up username belongings

    username_dict['games']=games_list
    all_games_for_users.append(username_dict)
    time.sleep(0)

r = json.dumps(all_games_for_users, ensure_ascii=False,indent='\t', separators=(',', ': '))
print(r)
f.write(r)
f.close()


    #print(next(games)['id'])
    #print(next(games)['id'])

